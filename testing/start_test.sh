#!/bin/bash

set -x

PACKAGES=( icinga-l10n )

if compgen -G "${WORKDIR}/${BUILDDIR}/RPMS/noarch/icinga-l10n-selinux*" > /dev/null; then
  PACKAGES+=( icinga-l10n-selinux )
fi

icinga-build-rpm-install "${PACKAGES[@]}"
